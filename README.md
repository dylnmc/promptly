promptly
========

### awesome ps1 prompts for bash

![screenshot of promptly in action](https://gitlab.com/dylnmc/promptly/raw/master/screenshot.png)

# setup

1. `cd git; git clone https://github.com/dylnmc/promptly`
    - you can, of course, `cd` where you'd like to first

2. `source $HOME/git/promptly/powerline.sh`
    - (where `$HOME/git` *might* be the location where you stored the promptly repo)
    - you can put the source in your .bashrc, for example

# TODO

* rewrite coming soon
    - easier configuration
    - better readme
    - more cool & useful themes! :3
