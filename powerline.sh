DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source ${DIR}/.attributes.sh

function toggle_ps1_dir_type {
    local _ret
    _ret="$?"
    sleep .1
    if [[ $_ps1_dir_type == 1 ]]; then
        _ps1_dir_type=2
    elif [[ $_ps1_dir_type == 2 ]]; then
        _ps1_dir_type=3
    else
        _ps1_dir_type='1'
    fi
}
# alt+d - toggle _ps1_dir_type
bind -x '"\ed":"toggle_ps1_dir_type from_bind"'


function _ps1_setup {
    # don't use non-ascii in tty
    [[ $(tty) == /dev/tty[0-9] ]] && _ps1_tty='1' || _ps1_tty=''

    [[ -z $_ps1_tty ]] && _ps1_divider='' || _ps1_divider='>'

    # pre (when output doesn't end on newline)
    [[ -z $_ps1_short_symbol ]] && _ps1_short_symbol='%'
    [[ -z $_ps1_short_at ]]     && _ps1_short_at="${f_b}${rev}"

    # ret (return of $?)
    [[ -z $_ps1_ret_fg ]] && _ps1_ret_fg="$f_w"
    [[ -z $_ps1_ret_bg ]] && _ps1_ret_bg="$b_r"
    [[ -z $_ps1_ret_at ]] && _ps1_ret_at="$bold"

    # dir (current working directory)
    [[ -z $_ps1_dir_type ]] && _ps1_dir_type='1'
    [[ -z $_ps1_dir_preserve ]] && _ps1_dir_preserve='30'
    [[ -z $_ps1_dir_hard_len ]] && _ps1_dir_hard_len='20'
    [[ -z $_ps1_dir_fg ]]   && _ps1_dir_fg="$f_b"
    [[ -z $_ps1_dir_bg ]]   && _ps1_dir_bg="$b_k"
    # only use italics outside screen/tmux
    # if [[ $TERM =~ ^screen ]] || [[ $TERM =~ ^tmux ]] || ! [[ $TERM =~ .*256color ]]; then
    #     [[ $_ps1_dir_at =~ $ITALIC ]] && _ps1_dir_at="$under"
    # else
    #     [[ -z $_ps1_dir_at ]] && _ps1_dir_at="$italic"
    # fi
    [[ -z $_ps1_dir_at ]] && _ps1_dir_at="$italic"

    # mid (main part)
    [[ -z $_ps1_user_symbol ]] && _ps1_user_symbol=" {user}"
    [[ -z $_ps1_at_symbol ]] && _ps1_at_symbol=" at"
    [[ -z $_ps1_host_symbol ]] && _ps1_host_symbol=" {host}"

    [[ -z $_ps1_user_fg ]] && _ps1_user_fg="$f_b"
    [[ -z $_ps1_user_bg ]] && _ps1_user_bg="$b_k"
    # [[ -z $_ps1_user_at ]] && _ps1_user_at=''

    [[ -z $_ps1_at_fg ]] && _ps1_at_fg="$f_w"
    [[ -z $_ps1_at_bg ]] && _ps1_at_bg="$b_k"
    # [[ -z $_ps1_at_at ]] && _ps1_at_at=''

    [[ -z $_ps1_host_fg ]] && _ps1_host_fg="$f_y"
    [[ -z $_ps1_host_bg ]] && _ps1_host_bg="$b_k"
    # [[ -z $_ps1_host_at ]] && _ps1_host_at=''

    # git
    [[ -z $_ps1_git_pre_symbol ]] && _ps1_git_pre_symbol=' '
    # [[ -z $_ps1_git_post_symbol ]] && _ps1_git_post_symbol=''

    [[ -z $_ps1_git_bg ]] && _ps1_git_bg=$b_k
    _ps1_git_fg_rev=$f_b
    _ps1_normal_fg_rev=$f_k

    [[ -z $_ps1_git_pre_fg ]] && _ps1_git_pre_fg=$f_b
    [[ -z $_ps1_git_pre_bg ]] && _ps1_git_pre_bg=$b_k
    # [[ -z $_ps1_git_pre_at ]] && _ps1_git_pre_at=''

    [[ -z $_ps1_git_branch_fg ]] && _ps1_git_branch_fg=$f_g
    [[ -z $_ps1_git_branch_bg ]] && _ps1_git_branch_bg=$b_k
    # [[ -z $_ps1_git_branch_at ]] && _ps1_git_pre_at=''

    [[ -z $_ps1_git_post_fg ]] && _ps1_git_post_fg=$f_b
    [[ -z $_ps1_git_post_bg ]] && _ps1_git_post_bg=$b_k
    # [[ -z $_ps1_git_post_at ]] && _ps1_git_pre_at=''

    [[ -z $_ps1_normal_fg_rev ]] && _ps1_normal_fg_rev=$f_k


    # make red if super user
    [[ $(id -u) == 0 ]] && _ps1_user_fg=$f_r

    PS1='$(_ps1 "\w" "\W" "$?" "\u" "\h")'
}

function _ps1 {
    # $1 = "\w" (pwd)
    # $2 = "\W" (short pwd)
    # $3 = "$?" (ret)
    # $4 = "\u" (user)
    # $5 = "\h" (host)

    exec < /dev/tty
    oldstty=$(stty -g) >/dev/null 2>&1
    stty raw -echo min 0 >/dev/null 2>&1
    echo -en "\033[6n" > /dev/tty 2>&1
    IFS=';' read -r -d R -a pos >/dev/null 2>&1
    stty $oldstty >/dev/null 2>&1
    row=$((${pos[0]:2} - 1)) >/dev/null 2>&1   # strip off the esc-[
    col=$((${pos[1]} - 1)) >/dev/null 2>&1

    _ps1_git_branch="$(git symbolic-ref --short -q HEAD 2> /dev/null)"
    _ps1_num_files=(!(.*))
    _ps1_num_dotfiles=(.!(.|))
    if [ $_ps1_num_files = '!(.*)' ]; then
        _ps1_num_files=0
    else
        _ps1_num_files="${#_ps1_num_files[@]}"
    fi
    if [ $_ps1_num_dotfiles = '.!(.|)' ]; then
        _ps1_num_dotfiles=0
    else
        _ps1_num_dotfiles="${#_ps1_num_dotfiles[@]}"
    fi

    _ps1_cur_dir="$1"
    if (( "$_ps1_dir_preserve" >= "0" )) 2>/dev/null; then
        while (( "${#_ps1_cur_dir}" > 20 )) && [[ "$_ps1_cur_dir" =~ \/[^\/]+\/ ]]; do
            _ps1_cur_dir="/${_ps1_cur_dir/${BASH_REMATCH[0]}/}"
        done
        [[ "$_ps1_cur_dir" != "$1" ]] && _ps1_cur_dir='..'"${_ps1_cur_dir}"
    else
        [[ "${#_ps1_cur_dir}" -gt "$_ps1_dir_hard_len" ]] && _ps1_cur_dir='..'"${_ps1_cur_dir:$(("${#_ps1_cur_dir}" - "$_ps1_dir_hard_len"))}"
    fi

    # short
    (( ${col:-2} > 1 )) >/dev/null 2>&1 && sleep .01 && printf '\001%s\002%s\001%s\002\n\001\r\002' "${reset}${_ps1_short_at}" "$_ps1_short_symbol" "$reset"

    # dir (type 2)
    [[ $_ps1_dir_type == 2 ]] &&\
        printf '\n\001\r%s\002 %s (%s .%s) \001%s\002%s\n\001\r\002'\
            "${rev}${_ps1_dir_at}${_ps1_dir_fg}${_ps1_dir_bg}"\
            "$1"\
            "$_ps1_num_files"\
            "$_ps1_num_dotfiles"\
            "${reset}${_ps1_dir_fg}"\
            "$_ps1_divider"

    # ret
    (( $3 != 0 )) &&\
        printf '\001%s\002%s\001%s\002%s\001%s\002'\
            "${_ps1_ret_at}${_ps1_ret_fg}${_ps1_ret_bg}"\
            "$3"\
            "${rev}${_ps1_ret_bg}${_ps1_normal_fg_rev}"\
            "$_ps1_divider"\
            "${reset}${_ps1_ret_bg}${_ps1_user_bg}"

     _user_symbol="${_ps1_user_symbol//\{user\}/$4}"
     _host_symbol="${_ps1_host_symbol//\{host\}/$5}"

    # mid
    printf '\001%s\002%s\001%s\002%s\001%s\002%s'\
        "${_ps1_user_at}${_ps1_user_fg}${_ps1_user_bg}"\
        "$_user_symbol"\
        "${_ps1_at_at}${_ps1_at_fg}${_ps1_at_bg}"\
        "$_ps1_at_symbol"\
        "${_ps1_host_at}${_ps1_host_fg}${_ps1_host_bg}"\
        "$_host_symbol"

    _dir_bg="$_ps1_host_bg"

    # git
    [[ -n $_ps1_git_branch ]] &&\
        printf "\001%s\002  \001%s\002%s\001%s\002%s\001%s\002%s\001%s\002"\
            "${_ps1_git_bg}"\
            "${_ps1_git_pre_at}${_ps1_git_pre_fg}${_ps1_git_pre_bg}"\
            "${_ps1_git_pre_symbol}"\
            "${_ps1_git_branch_at}${_ps1_git_branch_fg}${_ps1_git_branch_bg}"\
            "${_ps1_git_branch}"\
            "${_ps1_git_post_at}${_ps1_git_post_fg}${_ps1_git_post_bg}"\
            "${_ps1_git_post_symbol}"\
            "${_ps1_git_bg}"\
        &&\
            _dir_bg="$_ps1_git_post_bg"

    # dir (type 1)
    [[ $_ps1_dir_type == 1 ]] &&\
        printf ' \001%s\002%s\001%s\002 %s \001%s\002%s\001%s\002 '\
            "${rev}${_dir_bg}${_ps1_git_fg_rev}"\
            "$_ps1_divider"\
            "${_ps1_dir_at}${_ps1_dir_fg}${_ps1_dir_bg}"\
            "$_ps1_cur_dir"\
            "${reset}${_ps1_dir_fg}"\
            "$_ps1_divider"\
            "$reset"\
    ||\
        printf ' \001%s\002%s\001%s\002 '\
        "${reset}${_ps1_normal_fg_rev}"\
        "$_ps1_divider"\
        "$reset"
}

_ps1_setup
