DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
source ${DIR}/.attributes.sh

if [ -z "$GRML_DISPLAY_BATTERY" ]; then
    GRML_DISPLAY_BATTERY=1
fi

if [ -z "$GRML_GIT_BRACE_COLOR" ]; then
    GRML_GIT_BRACE_COLOR="$f_m"
fi

if [ -z "$GRML_GIT_BRANCH_COLOR" ]; then
    GRML_GIT_BRANCH_COLOR="$f_g"
fi

if [ -z "$GRML_ERROR_COLOR" ]; then
    GRML_ERROR_COLOR="$f_r"
fi

# battery dir
if [ -d /sys/class/power_supply/BAT0 ]; then
    _PS1_bat_dir='BAT0';
else
    _PS1_bat_dir='BAT1';
fi

# ps1 return and battery
_PS1_ret(){
    # should be at beg of line (otherwise more complex stuff needed)
    RET=$?;

    # battery
    if [[ "$GRML_DISPLAY_BATTERY" == "1" ]]; then
        if [ -d /sys/class/power_supply/$_PS1_bat_dir ]; then
            # linux
            STATUS="$( cat /sys/class/power_supply/$_PS1_bat_dir/status )";
            if [ "$STATUS" = "Discharging" ]; then
                bat=$( printf ' v%d%%' "$( cat /sys/class/power_supply/$_PS1_bat_dir/capacity )" );
            elif [ "$STATUS" = "Charging" ]; then
                bat=$( printf ' ^%d%%' "$( cat /sys/class/power_supply/$_PS1_bat_dir/capacity )" );
            elif [ "$STATUS" = "Full" ] || [ "$STATUS" = "Unknown" ] && [ "$(cat /sys/class/power_supply/$_PS1_bat_dir/capacity)" -gt "98" ]; then
                bat=$( printf ' =%d%%' "$( cat /sys/class/power_supply/$_PS1_bat_dir/capacity )" );
            else
                bat=$( printf ' ?%d%%' "$( cat /sys/class/power_supply/$_PS1_bat_dir/capacity )" );
            fi;
        fi
    fi

    if [[ "$RET" -ne "0" ]]; then
        printf '\001%*s%s\r%s\002%s ' "$(tput cols)" ":( $bat " "$GRML_ERROR_COLOR" "$RET"
    else
        printf '\001%*s%s\r\002' "$(tput cols)" "$bat "
    fi;
}

_HAS_GIT=$( type 'git' &> /dev/null );

# ps1 git branch
_PS1_git(){
    if ! $_HAS_GIT; then
        return 1;
    fi;
    if [ ! "$( git rev-parse --is-inside-git-dir 2> /dev/null )" ]; then
        return 2;
    fi
    branch="$( git symbolic-ref --short -q HEAD 2> /dev/null )"

    if [ "$branch" ]; then
        printf ' \001%s\002(\001%s\002git\001%s\002)\001%s\002-\001%s\002[\001%s\002%s\001%s\002]\001%s\002' "$GRML_GIT_BRACE_COLOR" "$c_off" "$GRML_GIT_BRACE_COLOR" "$c_off" "$GRML_GIT_BRACE_COLOR" "$GRML_GIT_BRANCH_COLOR" "${branch}" "$GRML_GIT_BRACE_COLOR" "$c_off"
    fi;
}

function row {
    local COL
    local ROW
    IFS=';' read -sdR -p $sep ROW COL
    echo "${ROW#*[}"
}

[[ $(id -u) == 0 ]] && user_f=$f_r || user_f=$f_b

# grml PS1 string
PS1="\n\[\$up\$reset\]\$(_PS1_ret)\[\$bold\$user_f\]${debian_chroot:+($debian_chroot)}\u\[\$reset\]@\h \[\$bold\]\w\$reset\$(_PS1_git) \[\$reset\]% "

