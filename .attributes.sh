# colors
# ~~~~~~
# fg
f_k="$(tput setaf 0)" # black
f_r="$(tput setaf 1)" # red
f_g="$(tput setaf 2)" # green
f_y="$(tput setaf 3)" # yellow
f_b="$(tput setaf 4)" # blue
f_m="$(tput setaf 5)" # magenta
f_c="$(tput setaf 6)" # cyan
f_w="$(tput setaf 7)" # white
# bg
b_k="$(tput setab 0)" # black
b_r="$(tput setab 1)" # red
b_g="$(tput setab 2)" # green
b_y="$(tput setab 3)" # yellow
b_b="$(tput setab 4)" # blue
b_m="$(tput setab 5)" # magenta
b_c="$(tput setab 6)" # cyan
b_w="$(tput setab 7)" # white
# both
# ~~~~
c_off="$(tput op)" # original pair (color off)

# atts
# ~~~~
reset="$(tput sgr0)" # atts off
bold="$(tput bold)" # bold
dim="$(tput dim)" # dim
italic="$(tput sitm)" # italic
under="$(tput smul)" # underline
blink="$(tput blink)" # blink
rev="$(tput rev)" # reverse
invis="$(tput invis)" # invisible

# cursor move
# ~~~~~~~~~~~
up="$(tput cuu1)"
down="$(tput cud1)"
back="$(tput cub1)"
forward="$(tput cuf1)"

# other
# ~~~~~
sep=$'\e[6n'
